<?xml version="1.0" encoding="utf-8" standalone="yes"?>

<xsl:stylesheet version="2.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:fn="http://www.w3.org/2005/xpath-functions"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:pbcore="http://www.pbcore.org/PBCore/PBCoreNamespace.html"
    xmlns="http://www.tessella.com/sdb/cmis/metadata"
    exclude-result-prefixes="pbcore">
    
    <xsl:output method='xml' indent='yes'/>
    
    <xsl:template match="pbcore:pbcoreDescriptionDocument">
        
        <group>
            <title>Document Description</title>
           
            <xsl:apply-templates select="pbcore:pbcoreIdentifier"><xsl:with-param name="name">Identifier</xsl:with-param><xsl:with-param name="type">creatorName</xsl:with-param></xsl:apply-templates>
            <xsl:apply-templates select="pbcore:pbcoreAssetType"><xsl:with-param name="name">Asset Type</xsl:with-param></xsl:apply-templates>
            <xsl:apply-templates select="pbcore:pbcoreAssetDate"><xsl:with-param name="name">Asset Date</xsl:with-param></xsl:apply-templates>
            <xsl:apply-templates select="pbcore:pbcoreCoverage/pbcore:coverage"><xsl:with-param name="name">Temporal Coverage</xsl:with-param></xsl:apply-templates>
            <xsl:apply-templates select="pbcore:pbcoreCoverage/pbcore:coverageType"><xsl:with-param name="name">Coverage Type</xsl:with-param></xsl:apply-templates>
            <xsl:apply-templates select="pbcore:pbcoreCoverage/pbcore:coverage"><xsl:with-param name="name">Geographic Coverage</xsl:with-param></xsl:apply-templates>
            <xsl:apply-templates select="pbcore:pbcoreCoverage/pbcore:coverageType"><xsl:with-param name="name">Coverage Type</xsl:with-param></xsl:apply-templates>
            <xsl:apply-templates select="pbcore:pbcoreCreator/pbcore:creator"><xsl:with-param name="name">Creator</xsl:with-param></xsl:apply-templates>
            <xsl:apply-templates select="pbcore:pbcoreCreator/pbcore:creatorRole"><xsl:with-param name="name">Creator Role</xsl:with-param></xsl:apply-templates>
            <xsl:apply-templates select="pbcore:pbcoreContributor/pbcore:contributor"><xsl:with-param name="name">Contributor name</xsl:with-param></xsl:apply-templates>
            <xsl:apply-templates select="pbcore:pbcoreContributor/pbcore:contributorRole"><xsl:with-param name="name">Contributor role</xsl:with-param></xsl:apply-templates>
            <xsl:apply-templates select="pbcore:pbcoreDescription"><xsl:with-param name="name">Description</xsl:with-param></xsl:apply-templates>
            <xsl:apply-templates select="pbcore:pbcorePublisher/pbcore:publisher"><xsl:with-param name="name">Publisher name</xsl:with-param></xsl:apply-templates>
            <xsl:apply-templates select="pbcore:pbcorePublisher/pbcore:publisherRole"><xsl:with-param name="name">Publisher role</xsl:with-param></xsl:apply-templates>
            
            <xsl:apply-templates select="pbcore:pbcoreRightsSummary/pbcore:rightsSummary"><xsl:with-param name="name">Rights summary</xsl:with-param></xsl:apply-templates>
            <xsl:apply-templates select="pbcore:pbcoreRightsSummary/pbcore:rightsLink"><xsl:with-param name="name">Rights statement URI</xsl:with-param></xsl:apply-templates>
            <xsl:apply-templates select="pbcore:pbcoreTitle"><xsl:with-param name="name">Title</xsl:with-param></xsl:apply-templates>
        </group>
        <xsl:apply-templates select="pbcore:pbcoreInstantiation"/>
    </xsl:template>
    
    <xsl:template match="pbcore:pbcoreInstantiation">
        
        <group>
            <title>Instantiation Description</title>
            <xsl:apply-templates select="pbcore:instantiationIdentifier"><xsl:with-param name="name">Instantiation Identifier</xsl:with-param></xsl:apply-templates>
            <xsl:apply-templates select="pbcore:instantiationLocation"><xsl:with-param name="name">Location</xsl:with-param></xsl:apply-templates>
            <xsl:apply-templates select="pbcore:instantiationFileSize[@unitsOfMeasure='MB']"><xsl:with-param name="name">File size</xsl:with-param></xsl:apply-templates>
            <xsl:apply-templates select="pbcore:instantiationDuration"><xsl:with-param name="name">Duration</xsl:with-param></xsl:apply-templates>
            <xsl:apply-templates select="pbcore:instantiationAlternativeModes"><xsl:with-param name="name">Alternatives</xsl:with-param></xsl:apply-templates>
            <xsl:apply-templates select="pbcore:instantiationLanguage[@source='ISO639.2']"><xsl:with-param name="name">Language</xsl:with-param></xsl:apply-templates>
            <xsl:apply-templates select="pbcore:rightsSummary"><xsl:with-param name="name">Rights Statement</xsl:with-param></xsl:apply-templates>
            <xsl:apply-templates select="pbcore:rightsLink"><xsl:with-param name="name">Rights Statement</xsl:with-param></xsl:apply-templates>
        </group>
    </xsl:template>
    
    <!--
    This template matches various PBCore elements, as listed in the match attribute.
    It outputs an item element: the name is passed in as a parameter of the template,
    and the value is the value of the PBcore element.
  -->
    
    
    <xsl:template match="pbcore:pbcoreIdentifier|pbcore:pbcoreAssetType|pbcore:pbcoreAssetDate|pbcore:coverage|pbcore:coverageType
        |pbcore:creator|pbcore:creatorRole|pbcore:contributor|pbcore:contributorRole|pbcore:pbcoreDescription|pbcore:publisher|pbcore:publisherRole|pbcore:rightsSummary|pbcore:rightsLink
        |pbcore:pbcoreInstantiation/pbcore:instantiationIdentifier|pbcore:pbcoreInstantiation/pbcore:pbcoreInstantiation|pbcore:pbcoreInstantiation/pbcore:instantiationFileSize|pbcore:pbcoreInstantiation/pbcore:instantiationDuration|pbcore:pbcoreInstantiation/pbcore:instantiationAlternativeModes
        |pbcore:pbcoreInstantiation/pbcore:instantiationLanguage|pbcore:rightsSummary|pbcore:rightsLink|pbcore:pbcoreTitle">
        <xsl:param name="name"/>
        <item>
            <name><xsl:value-of select="$name"/></name>
            <value><xsl:value-of select="text()"/></value>
            <type><xsl:value-of select="concat('pbcore.', local-name())"/></type>
        </item>
    </xsl:template>
    
</xsl:stylesheet>