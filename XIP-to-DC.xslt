<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:oai_dc="http://www.openarchives.org/OAI/2.0/oai_dc/" xmlns:ead="urn:isbn:1-931666-22-9" xmlns:mods="http://www.loc.gov/mods/v3" xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:pbcore="http://www.pbcore.org/PBCore/PBCoreNamespace.html" xmlns="http://www.tessella.com/sdb/cmis/metadata" version="2.0">
 <xsl:output method="xml" indent="yes"/>
 <!--  Root template  -->
 <xsl:template match="/">
  <xsl:apply-templates/>
 </xsl:template>
 <!--  Override default template for text nodes  -->
 <xsl:template match="text()|@*"/>


 <!-- 8-4-2020 This template matches the XIP entity elements (IO, CO) and embedded metadata and outputs Dublin Core. 
 Will need to be filled in with more DC elements-->
     
 
 <xsl:template match="//*[local-name()='XIP']/*[local-name()='Metadata']/*[local-name()='Content']">

  <xsl:if test="oai_dc:dc | dc:dc | pbcore:pbcore | mods:mods | ead:ead" >
  <records>
   <record>
   <metadata>
   <dc:title>
    <xsl:call-template name="assetTitle">
         <xsl:with-param name="defaultTitle" select="../../*[local-name()='InformationObject']/*[local-name()='Title']"/> 
    </xsl:call-template>
   </dc:title>
    <dc:creator>
     <xsl:call-template name="assetCreator"/>
    </dc:creator>
   <dc:description>
    <xsl:call-template name="assetDescription">
         <xsl:with-param name="defaultDescription" select="../../*[local-name()='InformationObject']/*[local-name()='Description']"/> 
    </xsl:call-template>
   </dc:description>
    <dc:date>
     <xsl:call-template name="assetDate"/>
    </dc:date>

     <xsl:call-template name="assetType">
      <xsl:with-param name="defaultType" select="../../*[local-name()='ContentObject']/*[local-name()='CustomType']"/> 
     </xsl:call-template>

  </metadata>
   </record>
  </records>
  </xsl:if>

 </xsl:template>
  
 <xsl:template name="assetTitle">
  <xsl:param name="defaultTitle"/>
  <xsl:choose>
   <xsl:when test="//dc:dc/dc:title">
    <xsl:value-of select="//dc:dc/dc:title"/>
   </xsl:when>
   <xsl:when test="//oai_dc:dc/dc:title">
    <xsl:value-of select="//oai_dc:dc/dc:title"/>
   </xsl:when>
   <xsl:when test="//pbcore:pbcoreDescriptionDocument/pbcore:pbcoreTitle">
    <xsl:value-of select="//pbcore:pbcoreDescriptionDocument/pbcore:pbcoreTitle"/>
   </xsl:when>
   <xsl:when test="//ead:ead/ead:archdesc/ead:did/ead:unittitle">
    <xsl:value-of select="//ead:ead/ead:archdesc/ead:did/ead:unittitle"/>
   </xsl:when>
   <xsl:when test="//mods:mods/mods:titleInfo/mods:title">
    <xsl:value-of select="//mods:mods/mods:titleInfo/mods:title"/>
   </xsl:when>
   <xsl:otherwise>
    <xsl:value-of select="$defaultTitle"/>
   </xsl:otherwise>
  </xsl:choose>
 </xsl:template>
  
 <xsl:template name="assetCreator">
  <xsl:choose>
   <xsl:when test="//dc:dc/dc:creator !=''">
    <xsl:value-of select="//dc:dc/dc:creator"/>
   </xsl:when>
   <xsl:when test="//oai_dc:dc/dc:creator !=''">
    <xsl:value-of select="//oai_dc:dc/dc:creator"/>
   </xsl:when>
   <xsl:when test="//pbcore:pbcoreDescriptionDocument/pbcore:pbcoreCreator/pbcore:creator !=''">
    <xsl:value-of select="//pbcore:pbcoreDescriptionDocument/pbcore:pbcoreCreator/pbcore:creator"/>
   </xsl:when>
   <xsl:when test="//ead:ead/ead:archdesc/ead:did/ead:origination !=''">
    <xsl:value-of select="//ead:ead/ead:archdesc/ead:did/ead:origination"/>
   </xsl:when>
   <xsl:when test="//mods:mods/mods:name/mods:namePart !=''">
    <xsl:value-of select="//mods:mods/mods:name/mods:namePart"/>
   </xsl:when>
   <xsl:otherwise>
    <xsl:text></xsl:text>
   </xsl:otherwise>
  </xsl:choose>
 </xsl:template>  
  
 <xsl:template name="assetDescription">
  <xsl:param name="defaultDescription"/>
  <xsl:choose>
   <xsl:when test="//dc:dc/dc:description">
    <xsl:value-of select="//dc:dc/dc:description"/>
   </xsl:when>
   <xsl:when test="//oai_dc:dc/dc:description">
    <xsl:value-of select="//oai_dc:dc/dc:description"/>
   </xsl:when>
   <xsl:when test="//pbcore:pbcoreDescriptionDocument/pbcore:pbcoreDescription">
    <xsl:value-of select="//pbcore:pbcoreDescriptionDocument/pbcore:pbcoreDescription"/>
   </xsl:when>
   <xsl:when test="//ead:ead/ead:archdesc/ead:did/ead:abstract">
    <xsl:value-of select="//ead:ead/ead:archdesc/ead:did/ead:abstract"/>
   </xsl:when>
   <xsl:when test="//mods:mods/mods:abstract">
    <xsl:value-of select="//mods:mods/mods:absract"/>
   </xsl:when>
   <xsl:otherwise>
    <xsl:value-of select="$defaultDescription"/>
   </xsl:otherwise>
  </xsl:choose>
 </xsl:template>
  
 <xsl:template name="assetDate">
  <xsl:choose>
   <xsl:when test="//dc:dc/dc:date">
    <xsl:value-of select="//dc:dc/dc:date"/>
   </xsl:when>
   <xsl:when test="//oai_dc:dc/dc:date">
    <xsl:value-of select="//oai_dc:dc/dc:date"/>
   </xsl:when>
   <xsl:when test="//pbcore:pbcoreDescriptionDocument/pbcore:pbcoreAssetDate">
    <xsl:value-of select="//pbcore:pbcoreDescriptionDocument/pbcore:pbcoreAssetDate"/>
   </xsl:when>
   <xsl:when test="//ead:ead/ead:archdesc/ead:did/ead:unitdate">
    <xsl:value-of select="//ead:ead/ead:archdesc/ead:did/ead:unitdate"/>
   </xsl:when>
   <xsl:when test="//mods:mods/mods:originInfo/mods:dateCreated">
    <xsl:value-of select="//mods:mods/mods:originInfo/mods:dateCreated"/>
   </xsl:when>
   <xsl:otherwise>
    <xsl:text></xsl:text> 
   </xsl:otherwise>
  </xsl:choose>
 </xsl:template>
 
 <xsl:template name="assetType">
  <xsl:param name="defaultType"/>
  <xsl:choose>
   <xsl:when test="//dc:dc/dc:type !=''">
    <xsl:for-each select="//dc:dc/dc:type">
     <dc:type>
      <xsl:value-of select="current()"/>
     </dc:type>
    </xsl:for-each>
   </xsl:when>
   <xsl:when test="//oai_dc:dc/dc:type !=''">
    <xsl:for-each select="//oai_dc:dc/dc:type">
    <dc:type>
     <xsl:value-of select="current()"/>
    </dc:type>
    </xsl:for-each>
   </xsl:when>
   <xsl:when test="//pbcore:pbcoreDescriptionDocument/pbcore:pbcoreInstantiation/pbcore:InstantiationMediaType !=''">
    <xsl:value-of select="//pbcore:pbcoreDescriptionDocument/pbcore:pbcoreInstantiation/pbcore:InstantiationMediaType"/>
   </xsl:when>
   <xsl:when test="//ead:ead">
    <xsl:text>archival resource</xsl:text>
   </xsl:when>
   <xsl:when test="//mods:mods/mods:typeOfResource !=''">
    <xsl:value-of select="//mods:mods/mods:typeOfResource"/>
   </xsl:when>
   <xsl:otherwise>
    <xsl:value-of select="$defaultType"/>
   </xsl:otherwise>
  </xsl:choose>
 </xsl:template>
 
</xsl:stylesheet>