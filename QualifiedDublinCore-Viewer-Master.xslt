<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="2.0" xmlns:dcterms="http://dublincore.org/documents/dcmi-terms/" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:sax="http://saxon.sf.net/">
 <xsl:output method="html" encoding="UTF-8"/>

 <!-- Parameter to locate the transform text file -->
 <xsl:param name="homeURI" />
 <!-- Default value for the language in case the locale is not passed to the transform -->
 <xsl:param name="xsltlanguage" select="'en-GB'"/>
 <!-- Accept an override for stylesheet location for local testing -->
 <xsl:param name="cssOverride" select="''"/>

 <!-- Value for this transform name, to select any transform specific text -->
 <xsl:variable name="xslt" select="'dcterms'"/>

 <!-- Import the XML file containing the locale specific text -->
 <xsl:variable name="stringFile" select="document(concat($homeURI,'/resources/transform_text.xml'))"/>
 <xsl:variable name="primaryLanguage" select="substring-before($xsltlanguage,'-')"/>

 <!-- Main entry point -->
 <xsl:template match="/">
  <div>

   <!-- Put the stylesheet override in here. -->
   <xsl:if test="$cssOverride">
    <link rel="stylesheet" type="text/css">
     <xsl:attribute name="href">
      <xsl:value-of select="$cssOverride"/>
     </xsl:attribute>
    </link>
   </xsl:if>

   <!-- Main Title for page -->
   <span class="XSLTransformTitle">
    <xsl:call-template name="getString">
     <xsl:with-param name="stringName" select="'mainHeader'"/>
    </xsl:call-template>
   </span>
   
   <br />
   <table class="XSLTransformTable">
    <col width="200px" />
    <col width="300px" />

    <xsl:call-template name="add-record-information" />
   </table>
  </div>
 </xsl:template>


 <!-- Template for the Record Information -->
 <xsl:template name="add-record-information">

  <!-- Heading for this section -->
  <xsl:call-template name="mainSectionHeaderTemplate">
   <xsl:with-param name="sectionName">
    <path>Qualified Dublin Core Metadata</path>    
   </xsl:with-param>
  </xsl:call-template>

  <!-- The individual fields -->

  <xsl:call-template name="add-field-node">
   <xsl:with-param name="field"><path>dcterms:dcterms/dcterms:title</path></xsl:with-param>
  </xsl:call-template>   
  <xsl:call-template name="add-field-node">
   <xsl:with-param name="field"><path>dcterms:dcterms/dcterms:title.alternative</path></xsl:with-param>
  </xsl:call-template>
  <xsl:call-template name="add-field-node">
   <xsl:with-param name="field"><path>dcterms:dcterms/dcterms:creator</path></xsl:with-param>
  </xsl:call-template>    
  <xsl:call-template name="add-field-node">
   <xsl:with-param name="field"><path>dcterms:dcterms/dcterms:contributor</path></xsl:with-param>
  </xsl:call-template>
  <xsl:call-template name="add-field-node">
   <xsl:with-param name="field"><path>dcterms:dcterms/dcterms:publisher</path></xsl:with-param>
  </xsl:call-template>  
  <xsl:call-template name="add-field-node">
   <xsl:with-param name="field"><path>dcterms:dcterms/dcterms:provenance</path></xsl:with-param>
  </xsl:call-template>
  <xsl:call-template name="add-field-node">
   <xsl:with-param name="field"><path>dcterms:dcterms/dcterms:language</path></xsl:with-param>
  </xsl:call-template>  
  <xsl:call-template name="add-field-node">
   <xsl:with-param name="field"><path>dcterms:dcterms/dcterms:subject</path></xsl:with-param>
  </xsl:call-template> 
  <xsl:call-template name="add-field-node">
   <xsl:with-param name="field"><path>dcterms:dcterms/dcterms:type</path></xsl:with-param>
  </xsl:call-template>
  
  <xsl:call-template name="mainSectionHeaderTemplate">
   <xsl:with-param name="sectionName">
    <path>Identifiers</path>
   </xsl:with-param>
  </xsl:call-template>
  <xsl:call-template name="add-field-node">
   <xsl:with-param name="field"><path>dcterms:dcterms/dcterms:identifier.filename</path></xsl:with-param>
  </xsl:call-template> 
  <xsl:call-template name="add-field-node">
   <xsl:with-param name="field"><path>dcterms:dcterms/dcterms:identifier.other</path></xsl:with-param>
  </xsl:call-template>
  <xsl:call-template name="add-field-node">
   <xsl:with-param name="field"><path>dcterms:dcterms/dcterms:identifier.bibliographicCitation</path></xsl:with-param>
  </xsl:call-template>


  <xsl:call-template name="mainSectionHeaderTemplate">
   <xsl:with-param name="sectionName">
    <path>Relations</path>
   </xsl:with-param>
  </xsl:call-template>
  <xsl:call-template name="add-field-node">
   <xsl:with-param name="field"><path>dcterms:dcterms/dcterms:relation.conformsTo</path></xsl:with-param>
  </xsl:call-template>
  <xsl:call-template name="add-field-node">
   <xsl:with-param name="field"><path>dcterms:dcterms/dcterms:relation.isPartOf</path></xsl:with-param>
  </xsl:call-template>
  <xsl:call-template name="add-field-node">
   <xsl:with-param name="field"><path>dcterms:dcterms/dcterms:relation.isReferencedBy</path></xsl:with-param>
  </xsl:call-template>
  <xsl:call-template name="add-field-node">
   <xsl:with-param name="field"><path>dcterms:dcterms/dcterms:relation.requires</path></xsl:with-param>
  </xsl:call-template>
  
  <xsl:call-template name="mainSectionHeaderTemplate">
   <xsl:with-param name="sectionName">
    <path>Date Information</path>
   </xsl:with-param>
  </xsl:call-template>
  <xsl:call-template name="add-field-node">
   <xsl:with-param name="field"><path>dcterms:dcterms/dcterms:date.created</path></xsl:with-param>
  </xsl:call-template>
  <xsl:call-template name="add-field-node">
   <xsl:with-param name="field"><path>dcterms:dcterms/dcterms:date.created.range</path></xsl:with-param>
  </xsl:call-template>
  <xsl:call-template name="add-field-node">
   <xsl:with-param name="field"><path>dcterms:dcterms/dcterms:date.digital</path></xsl:with-param>
  </xsl:call-template>
  
  <xsl:call-template name="mainSectionHeaderTemplate">
   <xsl:with-param name="sectionName">
    <path>Coverage Information</path>
   </xsl:with-param>
  </xsl:call-template>
  <xsl:call-template name="add-field-node">
   <xsl:with-param name="field"><path>dcterms:dcterms/dcterms:coverage.spatial</path></xsl:with-param>
  </xsl:call-template>
  <xsl:call-template name="add-field-node">
   <xsl:with-param name="field"><path>dcterms:dcterms/dcterms:coverage.temporal</path></xsl:with-param>
  </xsl:call-template>
  
  <xsl:call-template name="mainSectionHeaderTemplate">
   <xsl:with-param name="sectionName">
    <path>Descriptions</path>
   </xsl:with-param>
  </xsl:call-template>  
  <xsl:call-template name="add-big-field-node">
   <xsl:with-param name="field"><path>dcterms:dcterms/dcterms:description.abstract</path></xsl:with-param>
  </xsl:call-template>    
  <xsl:call-template name="add-big-field-node">
   <xsl:with-param name="field"><path>dcterms:dcterms/dcterms:description.tableOfContents</path></xsl:with-param>
  </xsl:call-template>
  
  <xsl:call-template name="mainSectionHeaderTemplate">
   <xsl:with-param name="sectionName">
    <path>Sources</path>
	</xsl:with-param>
  </xsl:call-template>
  <xsl:call-template name="add-field-node">
   <xsl:with-param name="field"><path>dcterms:dcterms/dcterms:source</path></xsl:with-param>
  </xsl:call-template>
  <xsl:call-template name="add-field-node">
   <xsl:with-param name="field"><path>dcterms:dcterms/dcterms:source.location</path></xsl:with-param>
  </xsl:call-template>
  <xsl:call-template name="add-field-node">
   <xsl:with-param name="field"><path>dcterms:dcterms/dcterms:source.volume</path></xsl:with-param>
  </xsl:call-template>
  
  <xsl:call-template name="mainSectionHeaderTemplate">
   <xsl:with-param name="sectionName">
    <path>Format info</path>
   </xsl:with-param>
  </xsl:call-template>
  <xsl:call-template name="add-field-node">
   <xsl:with-param name="field"><path>dcterms:dcterms/dcterms:format.medium</path></xsl:with-param>
  </xsl:call-template>
  <xsl:call-template name="add-field-node">
   <xsl:with-param name="field"><path>dcterms:dcterms/dcterms:format.extent</path></xsl:with-param>
  </xsl:call-template>
  
  <xsl:call-template name="mainSectionHeaderTemplate">
   <xsl:with-param name="sectionName">
    <path>Rights restrictions</path>
   </xsl:with-param>
  </xsl:call-template>  
  <xsl:call-template name="add-big-field-node">
   <xsl:with-param name="field"><path>dcterms:dcterms/dcterms:rights</path></xsl:with-param>
  </xsl:call-template>
  <xsl:call-template name="add-big-field-node">
   <xsl:with-param name="field"><path>dcterms:dcterms/dcterms:rights.accessRights</path></xsl:with-param>
  </xsl:call-template>  
 
 </xsl:template>
 
 <!-- Generic Template to add a field based on XPath Expression, whether or not it exists in the source XML-->
 <xsl:template name="add-field-node">
  <xsl:param name="field" />
  <xsl:choose>
   <xsl:when test="sax:evaluate($field)">
    <xsl:apply-templates select="sax:evaluate($field)">
     <xsl:with-param name="field">
      <xsl:value-of select="$field"/>
     </xsl:with-param>
    </xsl:apply-templates>
   </xsl:when>
   <xsl:otherwise>
    <xsl:call-template name="nameValuePairTemplate">
     <xsl:with-param name="field">
      <xsl:value-of select="$field"/>
     </xsl:with-param>
     <xsl:with-param name="fieldValue">
     </xsl:with-param>
    </xsl:call-template>
   </xsl:otherwise>
  </xsl:choose>
 </xsl:template>
 
 <!-- Generic Template to add a big field based on XPath Expression, whether or not it exists in the source XML-->
 <xsl:template name="add-big-field-node">
  <xsl:param name="field" />
  <xsl:choose>
   <xsl:when test="sax:evaluate($field)">
    <xsl:apply-templates select="sax:evaluate($field)">
     <xsl:with-param name="field">
      <xsl:value-of select="$field"/>
     </xsl:with-param>
    </xsl:apply-templates>
   </xsl:when>
   <xsl:otherwise>
    <xsl:call-template name="headingAndValueTemplate">
     <xsl:with-param name="field">
      <xsl:value-of select="$field"/>
     </xsl:with-param>
     <xsl:with-param name="fieldValue">
     </xsl:with-param>
    </xsl:call-template>
   </xsl:otherwise>
  </xsl:choose>
 </xsl:template>
 

 <!-- Template to deal with name value pair sections -->
  <!-- to handle dcterms:identifier subfields -->
  <xsl:template match="dcterms:identifier.filename">
   <xsl:param name="field"/>
   <xsl:call-template name="filename">
	<xsl:with-param name="field">
		<xsl:value-of select="$field"/>
	</xsl:with-param>
	<xsl:with-param name="fieldValue">
		<xsl:value-of select="text()"/>
	</xsl:with-param>
   </xsl:call-template>
  </xsl:template>
  <xsl:template name="filename">
        <xsl:param name="field" />
        <xsl:param name="fieldValue" />
        <tr>
            <td class="standardFieldName">
                <path>File Name</path>
            </td>
            <td class="standardFieldValue">
                <xsl:value-of select="$fieldValue" />
            </td>
        </tr>
    </xsl:template>
	  <xsl:template match="dcterms:identifier.other">
   <xsl:param name="field"/>
   <xsl:call-template name="other">
	<xsl:with-param name="field">
		<xsl:value-of select="$field"/>
	</xsl:with-param>
	<xsl:with-param name="fieldValue">
		<xsl:value-of select="text()"/>
	</xsl:with-param>
   </xsl:call-template>
  </xsl:template>
  <xsl:template name="other">
        <xsl:param name="field" />
        <xsl:param name="fieldValue" />
        <tr>
            <td class="standardFieldName">
                <path>Series or Collection Title</path>
            </td>
            <td class="standardFieldValue">
                <xsl:value-of select="$fieldValue" />
            </td>
        </tr>
    </xsl:template>
<xsl:template match="dcterms:identifier.bibliographicCitation">
   <xsl:param name="field"/>
   <xsl:call-template name="bibliographicCitation">
	<xsl:with-param name="field">
		<xsl:value-of select="$field"/>
	</xsl:with-param>
	<xsl:with-param name="fieldValue">
		<xsl:value-of select="text()"/>
	</xsl:with-param>
   </xsl:call-template>
  </xsl:template>
  <xsl:template name="bibliographicCitation">
        <xsl:param name="field" />
        <xsl:param name="fieldValue" />
        <tr>
            <td class="standardFieldName">
                <path>Preferred Citation</path>
            </td>
            <td class="standardFieldValue">
                <xsl:value-of select="$fieldValue" />
            </td>
        </tr>
    </xsl:template>
<!-- to handle dcterms:title -->
  <xsl:template match="dcterms:title">
   <xsl:param name="field"/>
   <xsl:call-template name="title">
	<xsl:with-param name="field">
		<xsl:value-of select="$field"/>
	</xsl:with-param>
	<xsl:with-param name="fieldValue">
		<xsl:value-of select="text()"/>
	</xsl:with-param>
   </xsl:call-template>
  </xsl:template>
  <xsl:template name="title">
        <xsl:param name="field" />
        <xsl:param name="fieldValue" />
        <tr>
            <td class="standardFieldName">
                <path>Title</path>
            </td>
            <td class="standardFieldValue">
                <xsl:value-of select="$fieldValue" />
            </td>
        </tr>
    </xsl:template>
<!-- to handle dcterms:title.alternative -->
  <xsl:template match="dcterms:title.alternative">
   <xsl:param name="field"/>
   <xsl:call-template name="alternative">
	<xsl:with-param name="field">
		<xsl:value-of select="$field"/>
	</xsl:with-param>
	<xsl:with-param name="fieldValue">
		<xsl:value-of select="text()"/>
	</xsl:with-param>
   </xsl:call-template>
  </xsl:template>
  <xsl:template name="alternative">
        <xsl:param name="field" />
        <xsl:param name="fieldValue" />
        <tr>
            <td class="standardFieldName">
                <path>Alternative Title</path>
            </td>
            <td class="standardFieldValue">
                <xsl:value-of select="$fieldValue" />
            </td>
        </tr>
    </xsl:template>
<!-- to deal with dcterms:creator -->
  <xsl:template match="dcterms:creator">
   <xsl:param name="field"/>
   <xsl:call-template name="creator">
	<xsl:with-param name="field">
		<xsl:value-of select="$field"/>
	</xsl:with-param>
	<xsl:with-param name="fieldValue">
		<xsl:value-of select="text()"/>
	</xsl:with-param>
   </xsl:call-template>
  </xsl:template>
  <xsl:template name="creator">
        <xsl:param name="field" />
        <xsl:param name="fieldValue" />
        <tr>
            <td class="standardFieldName">
                <path>Creator</path>
            </td>
            <td class="standardFieldValue">
                <xsl:value-of select="$fieldValue" />
            </td>
        </tr>
    </xsl:template>
<!-- to deal with dcterms:contributor -->
  <xsl:template match="dcterms:contributor">
   <xsl:param name="field"/>
   <xsl:call-template name="contributor">
	<xsl:with-param name="field">
		<xsl:value-of select="$field"/>
	</xsl:with-param>
	<xsl:with-param name="fieldValue">
		<xsl:value-of select="text()"/>
	</xsl:with-param>
   </xsl:call-template>
  </xsl:template>
  <xsl:template name="contributor">
        <xsl:param name="field" />
        <xsl:param name="fieldValue" />
        <tr>
            <td class="standardFieldName">
                <path>Contributor</path>
            </td>
            <td class="standardFieldValue">
                <xsl:value-of select="$fieldValue" />
            </td>
        </tr>
    </xsl:template>
<!-- to deal with dcterms:publisher -->
  <xsl:template match="dcterms:publisher">
   <xsl:param name="field"/>
   <xsl:call-template name="publisher">
	<xsl:with-param name="field">
		<xsl:value-of select="$field"/>
	</xsl:with-param>
	<xsl:with-param name="fieldValue">
		<xsl:value-of select="text()"/>
	</xsl:with-param>
   </xsl:call-template>
  </xsl:template>
  <xsl:template name="publisher">
        <xsl:param name="field" />
        <xsl:param name="fieldValue" />
        <tr>
            <td class="standardFieldName">
                <path>Publisher</path>
            </td>
            <td class="standardFieldValue">
                <xsl:value-of select="$fieldValue" />
            </td>
        </tr>
    </xsl:template>
<!-- to deal with dcterms:provenance -->
  <xsl:template match="dcterms:provenance">
   <xsl:param name="field"/>
   <xsl:call-template name="provenance">
	<xsl:with-param name="field">
		<xsl:value-of select="$field"/>
	</xsl:with-param>
	<xsl:with-param name="fieldValue">
		<xsl:value-of select="text()"/>
	</xsl:with-param>
   </xsl:call-template>
  </xsl:template>
  <xsl:template name="provenance">
        <xsl:param name="field" />
        <xsl:param name="fieldValue" />
        <tr>
            <td class="standardFieldName">
                <path>Record Group Title</path>
            </td>
            <td class="standardFieldValue">
                <xsl:value-of select="$fieldValue" />
            </td>
        </tr>
    </xsl:template>
<!-- to deal with dcterms:relations fields -->
<xsl:template match="dcterms:relation.conformsTo">
   <xsl:param name="field"/>
   <xsl:call-template name="conformsTo">
	<xsl:with-param name="field">
		<xsl:value-of select="$field"/>
	</xsl:with-param>
	<xsl:with-param name="fieldValue">
		<xsl:value-of select="text()"/>
	</xsl:with-param>
   </xsl:call-template>
  </xsl:template>
  <xsl:template name="conformsTo">
        <xsl:param name="field" />
        <xsl:param name="fieldValue" />
        <tr>
            <td class="standardFieldName">
                <path>Descriptive Standard</path>
            </td>
            <td class="standardFieldValue">
                <xsl:value-of select="$fieldValue" />
            </td>
        </tr>
    </xsl:template>
<xsl:template match="dcterms:relation.isPartOf">
   <xsl:param name="field"/>
   <xsl:call-template name="isPartOf">
	<xsl:with-param name="field">
		<xsl:value-of select="$field"/>
	</xsl:with-param>
	<xsl:with-param name="fieldValue">
		<xsl:value-of select="text()"/>
	</xsl:with-param>
   </xsl:call-template>
  </xsl:template>
  <xsl:template name="isPartOf">
        <xsl:param name="field" />
        <xsl:param name="fieldValue" />
        <tr>
            <td class="standardFieldName">
                <path>Hierarchy</path>
            </td>
            <td class="standardFieldValue">
                <xsl:value-of select="$fieldValue" />
            </td>
        </tr>
    </xsl:template>
  <xsl:template match="dcterms:relation.isReferencedBy">
   <xsl:param name="field"/>
   <xsl:call-template name="isReferencedBy">
	<xsl:with-param name="field">
		<xsl:value-of select="$field"/>
	</xsl:with-param>
	<xsl:with-param name="fieldValue">
		<xsl:value-of select="text()"/>
	</xsl:with-param>
   </xsl:call-template>
  </xsl:template>
  <xsl:template name="isReferencedBy">
        <xsl:param name="field" />
        <xsl:param name="fieldValue" />
        <tr>
            <td class="standardFieldName">
                <path>TARO URL</path>
            </td>
            <td class="standardFieldValue">
                <xsl:value-of select="$fieldValue" />
            </td>
        </tr>
    </xsl:template>
  <xsl:template match="dcterms:relation.requires">
   <xsl:param name="field"/>
   <xsl:call-template name="requires">
	<xsl:with-param name="field">
		<xsl:value-of select="$field"/>
	</xsl:with-param>
	<xsl:with-param name="fieldValue">
		<xsl:value-of select="text()"/>
	</xsl:with-param>
   </xsl:call-template>
  </xsl:template>
  <xsl:template name="requires">
        <xsl:param name="field" />
        <xsl:param name="fieldValue" />
        <tr>
            <td class="standardFieldName">
                <path>Requirements</path>
            </td>
            <td class="standardFieldValue">
                <xsl:value-of select="$fieldValue" />
            </td>
        </tr>
    </xsl:template>
<!-- to deal with dcterms:dates -->
  <xsl:template match="dcterms:date.created">
   <xsl:param name="field"/>
   <xsl:call-template name="created">
	<xsl:with-param name="field">
		<xsl:value-of select="$field"/>
	</xsl:with-param>
	<xsl:with-param name="fieldValue">
		<xsl:value-of select="text()"/>
	</xsl:with-param>
   </xsl:call-template>
  </xsl:template>
  <xsl:template name="created">
        <xsl:param name="field" />
        <xsl:param name="fieldValue" />
        <tr>
            <td class="standardFieldName">
                <path>Date of Creation</path>
            </td>
            <td class="standardFieldValue">
                <xsl:value-of select="$fieldValue" />
            </td>
        </tr>
    </xsl:template>
  <xsl:template match="dcterms:date.created.range">
   <xsl:param name="field"/>
   <xsl:call-template name="createdRange">
	<xsl:with-param name="field">
		<xsl:value-of select="$field"/>
	</xsl:with-param>
	<xsl:with-param name="fieldValue">
		<xsl:value-of select="text()"/>
	</xsl:with-param>
   </xsl:call-template>
  </xsl:template>
  <xsl:template name="createdRange">
        <xsl:param name="field" />
        <xsl:param name="fieldValue" />
        <tr>
            <td class="standardFieldName">
                <path>Date Range of Creation</path>
            </td>
            <td class="standardFieldValue">
                <xsl:value-of select="$fieldValue" />
            </td>
        </tr>
    </xsl:template>
<xsl:template match="dcterms:date.digital">
   <xsl:param name="field"/>
   <xsl:call-template name="digital">
	<xsl:with-param name="field">
		<xsl:value-of select="$field"/>
	</xsl:with-param>
	<xsl:with-param name="fieldValue">
		<xsl:value-of select="text()"/>
	</xsl:with-param>
   </xsl:call-template>
  </xsl:template>
  <xsl:template name="digital">
        <xsl:param name="field" />
        <xsl:param name="fieldValue" />
        <tr>
            <td class="standardFieldName">
                <path>Date Digitized</path>
            </td>
            <td class="standardFieldValue">
                <xsl:value-of select="$fieldValue" />
            </td>
        </tr>
    </xsl:template>
<!-- to deal with dcterms:coverages section -->
  <xsl:template match="dcterms:coverage.spatial">
   <xsl:param name="field"/>
   <xsl:call-template name="spatial">
	<xsl:with-param name="field">
		<xsl:value-of select="$field"/>
	</xsl:with-param>
	<xsl:with-param name="fieldValue">
		<xsl:value-of select="text()"/>
	</xsl:with-param>
   </xsl:call-template>
  </xsl:template>
  <xsl:template name="spatial">
        <xsl:param name="field" />
        <xsl:param name="fieldValue" />
        <tr>
            <td class="standardFieldName">
                <path>Geographic Coverage</path>
            </td>
            <td class="standardFieldValue">
                <xsl:value-of select="$fieldValue" />
            </td>
        </tr>
    </xsl:template>
  <xsl:template match="dcterms:coverage.temporal">
   <xsl:param name="field"/>
   <xsl:call-template name="temporal">
	<xsl:with-param name="field">
		<xsl:value-of select="$field"/>
	</xsl:with-param>
	<xsl:with-param name="fieldValue">
		<xsl:value-of select="text()"/>
	</xsl:with-param>
   </xsl:call-template>
  </xsl:template>
  <xsl:template name="temporal">
        <xsl:param name="field" />
        <xsl:param name="fieldValue" />
        <tr>
            <td class="standardFieldName">
                <path>Temporal Coverage</path>
            </td>
            <td class="standardFieldValue">
                <xsl:value-of select="$fieldValue" />
            </td>
        </tr>
    </xsl:template>
<!-- to deal with dcterms:descriptions -->
  <xsl:template match="dcterms:description.abstract">
   <xsl:param name="field"/>
   <xsl:call-template name="abstract">
	<xsl:with-param name="field">
		<xsl:value-of select="$field"/>
	</xsl:with-param>
	<xsl:with-param name="fieldValue">
		<xsl:value-of select="text()"/>
	</xsl:with-param>
   </xsl:call-template>
  </xsl:template>
  <xsl:template name="abstract">
        <xsl:param name="field" />
        <xsl:param name="fieldValue" />
        <tr>
            <td colspan="2" class="subSectionHeader">
                <path>Description</path>
            </td>
		</tr>
		<tr>
            <td colspan="2" class="standardFieldValue">
                <xsl:value-of select="$fieldValue" />
            </td>
        </tr>
	</xsl:template>
  <xsl:template match="dcterms:description.tableOfContents">
   <xsl:param name="field"/>
   <xsl:call-template name="tableOfContents">
	<xsl:with-param name="field">
		<xsl:value-of select="$field"/>
	</xsl:with-param>
	<xsl:with-param name="fieldValue">
		<xsl:value-of select="text()"/>
	</xsl:with-param>
   </xsl:call-template>
  </xsl:template>
  <xsl:template name="tableOfContents">
        <xsl:param name="field" />
        <xsl:param name="fieldValue" />
        <tr>
            <td colspan="2" class="subSectionHeader">
                <path>Arrangement</path>
            </td>
		</tr>
		<tr>
            <td colspan="2" class="standardFieldValue">
                <xsl:value-of select="$fieldValue" />
            </td>
        </tr>
	</xsl:template>
<!-- to deal with dcterms:sources -->
  <xsl:template match="dcterms:source">
   <xsl:param name="field"/>
   <xsl:call-template name="source">
	<xsl:with-param name="field">
		<xsl:value-of select="$field"/>
	</xsl:with-param>
	<xsl:with-param name="fieldValue">
		<xsl:value-of select="text()"/>
	</xsl:with-param>
   </xsl:call-template>
  </xsl:template>
  <xsl:template name="source">
        <xsl:param name="field" />
        <xsl:param name="fieldValue" />
        <tr>
            <td class="standardFieldName">
                <path>Source</path>
            </td>
            <td class="standardFieldValue">
                <xsl:value-of select="$fieldValue" />
            </td>
        </tr>
    </xsl:template>
  <xsl:template match="dcterms:source.location">
   <xsl:param name="field"/>
   <xsl:call-template name="location">
	<xsl:with-param name="field">
		<xsl:value-of select="$field"/>
	</xsl:with-param>
	<xsl:with-param name="fieldValue">
		<xsl:value-of select="text()"/>
	</xsl:with-param>
   </xsl:call-template>
  </xsl:template>
  <xsl:template name="location">
        <xsl:param name="field" />
        <xsl:param name="fieldValue" />
        <tr>
            <td class="standardFieldName">
                <path>Source Location</path>
            </td>
            <td class="standardFieldValue">
                <xsl:value-of select="$fieldValue" />
            </td>
        </tr>
    </xsl:template>
  <xsl:template match="dcterms:source.volume">
   <xsl:param name="field"/>
   <xsl:call-template name="volume">
	<xsl:with-param name="field">
		<xsl:value-of select="$field"/>
	</xsl:with-param>
	<xsl:with-param name="fieldValue">
		<xsl:value-of select="text()"/>
	</xsl:with-param>
   </xsl:call-template>
  </xsl:template>
  <xsl:template name="volume">
        <xsl:param name="field" />
        <xsl:param name="fieldValue" />
        <tr>
            <td class="standardFieldName">
                <path>Book Volume</path>
            </td>
            <td class="standardFieldValue">
                <xsl:value-of select="$fieldValue" />
            </td>
        </tr>
    </xsl:template>
<!-- to deal with dcterms:language section -->
  <xsl:template match="dcterms:language">
   <xsl:param name="field"/>
   <xsl:call-template name="language">
	<xsl:with-param name="field">
		<xsl:value-of select="$field"/>
	</xsl:with-param>
	<xsl:with-param name="fieldValue">
		<xsl:value-of select="text()"/>
	</xsl:with-param>
   </xsl:call-template>
  </xsl:template>
  <xsl:template name="language">
        <xsl:param name="field" />
        <xsl:param name="fieldValue" />
        <tr>
            <td class="standardFieldName">
                <path>Language</path>
            </td>
            <td class="standardFieldValue">
                <xsl:value-of select="$fieldValue" />
            </td>
        </tr>
    </xsl:template>
<!-- to deal with dcterms:subject -->
  <xsl:template match="dcterms:subject">
   <xsl:param name="field"/>
   <xsl:call-template name="subject">
	<xsl:with-param name="field">
		<xsl:value-of select="$field"/>
	</xsl:with-param>
	<xsl:with-param name="fieldValue">
		<xsl:value-of select="text()"/>
	</xsl:with-param>
   </xsl:call-template>
  </xsl:template>
  <xsl:template name="subject">
        <xsl:param name="field" />
        <xsl:param name="fieldValue" />
        <tr>
            <td class="standardFieldName">
                <path>Subject</path>
            </td>
            <td class="standardFieldValue">
                <xsl:value-of select="$fieldValue" />
            </td>
        </tr>
    </xsl:template>
<!-- to deal with dcterms:type -->
  <xsl:template match="dcterms:type">
   <xsl:param name="field"/>
   <xsl:call-template name="type">
	<xsl:with-param name="field">
		<xsl:value-of select="$field"/>
	</xsl:with-param>
	<xsl:with-param name="fieldValue">
		<xsl:value-of select="text()"/>
	</xsl:with-param>
   </xsl:call-template>
  </xsl:template>
  <xsl:template name="type">
        <xsl:param name="field" />
        <xsl:param name="fieldValue" />
        <tr>
            <td class="standardFieldName">
                <path>Mime or Record Type</path>
            </td>
            <td class="standardFieldValue">
                <xsl:value-of select="$fieldValue" />
            </td>
        </tr>
    </xsl:template>
<!-- to deal with dcterms:formats section -->
  <xsl:template match="dcterms:format.medium">
   <xsl:param name="field"/>
   <xsl:call-template name="medium">
	<xsl:with-param name="field">
		<xsl:value-of select="$field"/>
	</xsl:with-param>
	<xsl:with-param name="fieldValue">
		<xsl:value-of select="text()"/>
	</xsl:with-param>
   </xsl:call-template>
  </xsl:template>
  <xsl:template name="medium">
        <xsl:param name="field" />
        <xsl:param name="fieldValue" />
        <tr>
            <td class="standardFieldName">
                <path>Physical Format</path>
            </td>
            <td class="standardFieldValue">
                <xsl:value-of select="$fieldValue" />
            </td>
        </tr>
    </xsl:template>
  <xsl:template match="dcterms:format.extent">
   <xsl:param name="field"/>
   <xsl:call-template name="extent">
	<xsl:with-param name="field">
		<xsl:value-of select="$field"/>
	</xsl:with-param>
	<xsl:with-param name="fieldValue">
		<xsl:value-of select="text()"/>
	</xsl:with-param>
   </xsl:call-template>
  </xsl:template>
  <xsl:template name="extent">
        <xsl:param name="field" />
        <xsl:param name="fieldValue" />
        <tr>
            <td class="standardFieldName">
                <path>Extent</path>
            </td>
            <td class="standardFieldValue">
                <xsl:value-of select="$fieldValue" />
            </td>
        </tr>
    </xsl:template>
<!-- to deal with dcterms:rights big value section -->
  <xsl:template match="dcterms:rights">
   <xsl:param name="field"/>
   <xsl:call-template name="rights">
	<xsl:with-param name="field">
		<xsl:value-of select="$field"/>
	</xsl:with-param>
	<xsl:with-param name="fieldValue">
		<xsl:value-of select="text()"/>
	</xsl:with-param>
   </xsl:call-template>
  </xsl:template>
  <xsl:template name="rights">
        <xsl:param name="field" />
        <xsl:param name="fieldValue" />
        <tr>
            <td colspan="2" class="subSectionHeader">
                <path>Rights</path>
            </td>
		</tr>
		<tr>
            <td colspan="2" class="standardFieldValue">
                <xsl:value-of select="$fieldValue" />
            </td>
        </tr>
    </xsl:template>
<!-- to deal with dcterms:rights big value section -->
  <xsl:template match="dcterms:rights.accessRights">
   <xsl:param name="field"/>
   <xsl:call-template name="accessRights">
	<xsl:with-param name="field">
		<xsl:value-of select="$field"/>
	</xsl:with-param>
	<xsl:with-param name="fieldValue">
		<xsl:value-of select="text()"/>
	</xsl:with-param>
   </xsl:call-template>
  </xsl:template>
  <xsl:template name="accessRights">
        <xsl:param name="field" />
        <xsl:param name="fieldValue" />
        <tr>
            <td colspan="2" class="subSectionHeader">
                <path>Access Rights</path>
            </td>
		</tr>
		<tr>
            <td colspan="2" class="standardFieldValue">
                <xsl:value-of select="$fieldValue" />
            </td>
        </tr>
    </xsl:template>

<!-- original formatting of the rendering	
 <xsl:template match="dcterms:source">
 <xsl:param name="field" />

  <xsl:call-template name="nameValuePairTemplate">
   <xsl:with-param name="field">
    <xsl:value-of select="$field"/>
   </xsl:with-param>
   <xsl:with-param name="fieldValue">
    <xsl:value-of select="text()"/>
   </xsl:with-param>
  </xsl:call-template>
 </xsl:template>
 -->

 <!-- Template to deal with big value sections -->
 <!-- original dcterms:rights viewer
 <xsl:template match="dcterms:rights">
 <xsl:param name="field"/>
  
  <xsl:call-template name="headingAndValueTemplate">
   <xsl:with-param name="field">
    <xsl:value-of select="$field"/>
   </xsl:with-param>
   <xsl:with-param name="fieldValue">
    <xsl:value-of select="text()"/>
   </xsl:with-param>
  </xsl:call-template>
 </xsl:template> 
-->


 <!-- OUTPUT TEMPLATES -->

 <!-- Template to show a main section header -->
 <xsl:template name="mainSectionHeaderTemplate">
  <xsl:param name="sectionName"/>
  <tr>
   <td colspan="2" class="mainSectionHeader">
    <xsl:value-of select="$sectionName"/>
   </td>
  </tr>
 </xsl:template>

 <!-- Template to show a name value pair -->
 <xsl:template name="nameValuePairTemplate">
  <xsl:param name="field"/>
  <xsl:param name="fieldValue"/>

  <tr>
   <td class="standardFieldName">
    <xsl:call-template name="getString">
     <xsl:with-param name="stringName">
      <xsl:value-of select="$field"/>
     </xsl:with-param>
    </xsl:call-template>
   </td>
   <td class="standardFieldValue">
    <xsl:value-of select="$fieldValue"/>
   </td>
  </tr>
 </xsl:template>

 
 <!-- Template to show a heading and value field -->
 <xsl:template name="headingAndValueTemplate">
  <xsl:param name="field"/>
  <xsl:param name="fieldValue"/>

  <tr>
   <td colspan="2" class="subSectionHeader">
    <xsl:call-template name="getString">
     <xsl:with-param name="stringName">
      <xsl:value-of select="$field"/>
     </xsl:with-param>
    </xsl:call-template>
   </td>
  </tr>
  <tr>
   <td colspan="2" class="standardFieldValue">
    <xsl:value-of select="$fieldValue" />
   </td>
  </tr>
 </xsl:template>
 
 
 

 <!-- Utilities -->

 <!-- Define an element for hardcoded paths - a normalized string -->
 <xs:element name="xpath" type="xs:normalizedString" />

 <!-- Variable to introduce a spacer row in the output where required -->
 <xsl:variable name="spacerRow">
  <tr class="spacer" colspan="2">
   <td />
  </tr>
 </xsl:variable>

 <!-- Template method used to localise the XSLT transform -->
 <xsl:template name="getString">
  <xsl:param name="stringName"/>
  <xsl:variable name="str" select="$stringFile/strings/str[@name=$stringName][@xslt=$xslt or not(@xslt)]"/>
  <xsl:choose>
   <!-- case where the locale passed as param had format "en" -->
   <xsl:when test="$str[lang($xsltlanguage)]">
    <xsl:value-of select="$str[lang($xsltlanguage)][1]"/>
   </xsl:when>
   <!-- case where the locale passed as param had format "en-GB" -->
   <xsl:when test="$str[lang($primaryLanguage)]">
    <xsl:value-of select="$str[lang($primaryLanguage)][1]"/>
   </xsl:when>
   <!-- issue a warning if no translation found -->
   <xsl:otherwise>
    <xsl:message terminate="no">
     <xsl:text>Warning: no string named '</xsl:text>
     <xsl:value-of select="$stringName"/>
     <xsl:text>' found.</xsl:text>
    </xsl:message>
   </xsl:otherwise>
  </xsl:choose>
 </xsl:template>

</xsl:stylesheet>
