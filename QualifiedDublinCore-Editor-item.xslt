<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="2.0" xmlns:dcterms="http://dublincore.org/documents/dcmi-terms/" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:sax="http://saxon.sf.net/">
 <xsl:output method="html" encoding="UTF-8"/>

 <!-- Parameter to locate the transform text file -->
 <xsl:param name="homeURI" />
 <!-- Default value for the language in case the locale is not passed to the transform -->
 <xsl:param name="xsltlanguage" select="'en-GB'"/>
 <!-- Accept an override for stylesheet location for local testing -->
 <xsl:param name="cssOverride" select="''"/>

 <!-- Value for this transform name, to select any transform specific text -->
 <xsl:variable name="xslt" select="'dcterms'"/>

 <!-- Import the XML file containing the locale specific text -->
 <xsl:variable name="stringFile" select="document(concat($homeURI,'/resources/transform_text.xml'))"/>
 <xsl:variable name="primaryLanguage" select="substring-before($xsltlanguage,'-')"/>

 <!-- Main entry point -->
 <xsl:template match="/">
  <div>

   <!-- Put the stylesheet override in here. -->
   <xsl:if test="$cssOverride">
    <link rel="stylesheet" type="text/css">
     <xsl:attribute name="href">
      <xsl:value-of select="$cssOverride"/>
     </xsl:attribute>
    </link>
   </xsl:if>

   <!-- Main Title for page -->
   <span class="XSLTransformTitle">
    <xsl:call-template name="getString">
     <xsl:with-param name="stringName" select="'mainHeader'"/>
    </xsl:call-template>
   </span>
   
   <br />
   <table class="XSLTransformTable">
    <col width="200px" />
    <col width="300px" />

    <xsl:call-template name="add-record-information" />
   </table>
  </div>
 </xsl:template>


 <!-- Template for the Record Information -->
 <xsl:template name="add-record-information">

  <!-- Heading for this section -->
  <xsl:call-template name="mainSectionHeaderTemplate">
   <xsl:with-param name="sectionName">
	<path>Qualified Dublin Core Metadata</path>
   </xsl:with-param>
  </xsl:call-template>

  <!-- The individual fields -->
  <xsl:call-template name="mainSectionHeaderTemplate">
   <xsl:with-param name="sectionName">
    <path>Metadata Fields</path>
   </xsl:with-param>
  </xsl:call-template>

  <xsl:call-template name="add-field-node">
   <xsl:with-param name="field"><path>dcterms:dcterms/dcterms:identifier.bibliographicCitation</path></xsl:with-param>
  </xsl:call-template>


 </xsl:template>
 
 <!-- Generic Template to add a field based on XPath Expression, whether or not it exists in the source XML-->
 <xsl:template name="add-field-node">
  <xsl:param name="field"/>
  <xsl:param name="type"/>
  <xsl:choose>
   <xsl:when test="sax:evaluate($field)">
    <xsl:apply-templates select="sax:evaluate($field)">
     <xsl:with-param name="field">
      <xsl:value-of select="$field"/>
     </xsl:with-param>
    </xsl:apply-templates>
   </xsl:when>
   <xsl:otherwise>
    <xsl:choose>
     <xsl:when test="$type='date'">
      <xsl:call-template name="nameValuePairTemplateDate">
       <xsl:with-param name="field">
        <xsl:value-of select="$field"/>
       </xsl:with-param>
       <xsl:with-param name="fieldValue">
       </xsl:with-param>
      </xsl:call-template>
     </xsl:when>
     <xsl:otherwise>
      <xsl:call-template name="nameValuePairTemplateText">
       <xsl:with-param name="field">
        <xsl:value-of select="$field"/>
       </xsl:with-param>
       <xsl:with-param name="fieldValue">
       </xsl:with-param>
      </xsl:call-template>
     </xsl:otherwise>
    </xsl:choose>
   </xsl:otherwise>
  </xsl:choose>
 </xsl:template>


 <!-- Generic Template to add a big field based on XPath Expression, whether or not it exists in the source XML-->
 <xsl:template name="add-big-field-node">
  <xsl:param name="field"/>
  <xsl:choose>
   <xsl:when test="sax:evaluate($field)">
    <xsl:apply-templates select="sax:evaluate($field)">
     <xsl:with-param name="field">
      <xsl:value-of select="$field"/>
     </xsl:with-param>
    </xsl:apply-templates>
   </xsl:when>
   <xsl:otherwise>
    <xsl:call-template name="headingAndValueTemplate">
     <xsl:with-param name="field">
      <xsl:value-of select="$field"/>
     </xsl:with-param>
     <xsl:with-param name="fieldValue">
     </xsl:with-param>
    </xsl:call-template>
   </xsl:otherwise>
  </xsl:choose>
 </xsl:template>





 <!-- Template to deal with name value pair text sections -->
 <!-- section to deal with dcterms:identifiers -->

<xsl:template match="dcterms:identifier.bibliographicCitation">
   <xsl:param name="field"/>
   <xsl:call-template name="bibliographicCitation">
	<xsl:with-param name="field">
		<xsl:value-of select="$field"/>
	</xsl:with-param>
	<xsl:with-param name="fieldValue">
		<xsl:value-of select="text()"/>
	</xsl:with-param>
   </xsl:call-template>
  </xsl:template>
  <xsl:template name="bibliographicCitation">
        <xsl:param name="field" />
        <xsl:param name="fieldValue" />
        <tr>
            <td class="standardFieldName">
                <path>Preferred Citation</path>
            </td>
            <td>
				<xsl:attribute name="id">editable-<xsl:value-of select="generate-id(.)"/>
				</xsl:attribute>
				<input type="text">
					<xsl:attribute name="name">
						<xsl:value-of select="$field"/>
					</xsl:attribute>    
					<xsl:attribute name="value">
						<xsl:value-of select="$fieldValue"/>
					</xsl:attribute>
					<xsl:attribute name="class">
						<xsl:text>standardTextFieldInput</xsl:text>
					</xsl:attribute>
				</input>
			</td>
        </tr>
</xsl:template>

<!-- to deal with dcterms:relations fields -->

<xsl:template match="dcterms:relation.isPartOf">
   <xsl:param name="field"/>
   <xsl:call-template name="isPartOf">
	<xsl:with-param name="field">
		<xsl:value-of select="$field"/>
	</xsl:with-param>
	<xsl:with-param name="fieldValue">
		<xsl:value-of select="text()"/>
	</xsl:with-param>
   </xsl:call-template>
  </xsl:template>
  <xsl:template name="isPartOf">
        <xsl:param name="field" />
        <xsl:param name="fieldValue" />
        <tr>
            <td class="standardFieldName">
                <path>Hierarchy</path>
            </td>
            <td>
				<xsl:attribute name="id">editable-<xsl:value-of select="generate-id(.)"/>
				</xsl:attribute>
				<input type="text">
					<xsl:attribute name="name">
						<xsl:value-of select="$field"/>
					</xsl:attribute>    
					<xsl:attribute name="value">
						<xsl:value-of select="$fieldValue"/>
					</xsl:attribute>
					<xsl:attribute name="class">
						<xsl:text>standardTextFieldInput</xsl:text>
					</xsl:attribute>
				</input>
			</td>
        </tr>
    </xsl:template>

<!-- original method of editing
 <xsl:template match="dcterms:source">
 <xsl:param name="field" />
  <xsl:call-template name="nameValuePairTemplateText">
   <xsl:with-param name="field">
    <xsl:value-of select="$field"/>
   </xsl:with-param>
   <xsl:with-param name="fieldValue">
    <xsl:value-of select="text()"/>
   </xsl:with-param>
  </xsl:call-template>
 </xsl:template>
-->
 <!-- Template to deal with date sections -->

 <!-- Template to deal with big value sections -->
 <!--
 <xsl:template match="dcterms:accessRights|dcterms:rightsHolder">
 <xsl:param name="field"/>
  
  <xsl:call-template name="headingAndValueTemplate">
   <xsl:with-param name="field">
    <xsl:value-of select="$field"/>
   </xsl:with-param>
   <xsl:with-param name="fieldValue">
    <xsl:value-of select="text()"/>
   </xsl:with-param>
  </xsl:call-template>
 </xsl:template>
 -->



 <!-- OUTPUT TEMPLATES -->

 <!-- Template to show a main section header -->
 <xsl:template name="mainSectionHeaderTemplate">
  <xsl:param name="sectionName"/>
  <tr>
   <td colspan="2" class="mainSectionHeader">
    <xsl:value-of select="$sectionName"/>
   </td>
  </tr>
 </xsl:template>
 
 <!-- Template to show a name value pair (text mode) -->
 <xsl:template name="nameValuePairTemplateText">
  <xsl:param name="field"/>
  <xsl:param name="fieldValue"/>

  <tr>
   <td class="standardFieldName">
    <xsl:call-template name="getString">
     <xsl:with-param name="stringName">
      <xsl:value-of select="$field"/>
     </xsl:with-param>
    </xsl:call-template>
   </td>

   <td>
    <xsl:attribute name="id">
     editable-<xsl:value-of select="generate-id(.)"/>
    </xsl:attribute>
    <input type="text">
     <xsl:attribute name="name">
      <xsl:value-of select="$field"/>
     </xsl:attribute>    
     <xsl:attribute name="value">
      <xsl:value-of select="$fieldValue"/>
     </xsl:attribute>
     <xsl:attribute name="class">
      <xsl:text>standardTextFieldInput</xsl:text>
     </xsl:attribute>
    </input>
   </td>
  </tr>
 </xsl:template>
 
 <!-- Template to show a name value pair (date mode) -->
 <xsl:template name="nameValuePairTemplateDate">
  <xsl:param name="field"/>
  <xsl:param name="fieldValue"/>

  <tr>
   <td class="standardFieldName">
    <xsl:call-template name="getString">
     <xsl:with-param name="stringName">
      <xsl:value-of select="$field"/>
     </xsl:with-param>
    </xsl:call-template>
   </td>

   <td>
    <xsl:attribute name="id">
     editable-<xsl:value-of select="generate-id(.)"/>
    </xsl:attribute>
    <input type="date">
     <xsl:attribute name="name">
      <xsl:value-of select="$field"/>
     </xsl:attribute>    
     <xsl:attribute name="value">
      <xsl:value-of select="$fieldValue"/>
     </xsl:attribute>
     <xsl:attribute name="class">
      <xsl:text>standardTextFieldInput</xsl:text>
     </xsl:attribute>
    </input>
   </td>
  </tr>
 </xsl:template> 

 
 <!-- Template to show a heading and value field -->
 <xsl:template name="headingAndValueTemplate">
  <xsl:param name="field"/>
  <xsl:param name="fieldValue"/>

  <tr>
   <td colspan="2" class="subSectionHeader">
    <xsl:call-template name="getString">
     <xsl:with-param name="stringName">
      <xsl:value-of select="$field"/>
     </xsl:with-param>
    </xsl:call-template>
   </td>
  </tr>

  <tr>
   <td colspan="2">
    <xsl:attribute name="id">
     editable-<xsl:value-of select="generate-id(.)"/>
    </xsl:attribute>
    <textArea rows="4" cols="80" class="standardTextAreaInput">
     <xsl:attribute name="name">
      <xsl:value-of select="$field"/>
     </xsl:attribute>
		<xsl:choose>
			<xsl:when test="string-length($fieldValue)!=0">
				<xsl:value-of select="$fieldValue"/>
			</xsl:when>
			<xsl:otherwise>...</xsl:otherwise>
		</xsl:choose>
    </textArea>
   </td>
  </tr>
 </xsl:template>
 
 
 

 <!-- Utilities -->

 <!-- Define an element for hardcoded paths - a normalized string -->
 <xs:element name="xpath" type="xs:normalizedString" />

 <!-- Variable to introduce a spacer row in the output where required -->
 <xsl:variable name="spacerRow">
  <tr class="spacer" colspan="2">
   <td />
  </tr>
 </xsl:variable>

 <!-- Template method used to localise the XSLT transform -->
 <xsl:template name="getString">
  <xsl:param name="stringName"/>
  <xsl:variable name="str" select="$stringFile/strings/str[@name=$stringName][@xslt=$xslt or not(@xslt)]"/>
  <xsl:choose>
   <!-- case where the locale passed as param had format "en" -->
   <xsl:when test="$str[lang($xsltlanguage)]">
    <xsl:value-of select="$str[lang($xsltlanguage)][1]"/>
   </xsl:when>
   <!-- case where the locale passed as param had format "en-GB" -->
   <xsl:when test="$str[lang($primaryLanguage)]">
    <xsl:value-of select="$str[lang($primaryLanguage)][1]"/>
   </xsl:when>
   <!-- issue a warning if no translation found -->
   <xsl:otherwise>
    <xsl:message terminate="no">
     <xsl:text>Warning: no string named '</xsl:text>
     <xsl:value-of select="$stringName"/>
     <xsl:text>' found.</xsl:text>
    </xsl:message>
   </xsl:otherwise>
  </xsl:choose>
 </xsl:template>

</xsl:stylesheet>
