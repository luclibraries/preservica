<?xml version="1.0" encoding="utf-8" standalone="yes"?>
<xsl:stylesheet version="2.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:fn="http://www.w3.org/2005/xpath-functions"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:dcterms="http://dublincore.org/documents/dcmi-terms/"
    xmlns="http://www.tessella.com/sdb/cmis/metadata"
    exclude-result-prefixes="dcterms">

  <xsl:output method='xml' indent='yes'/>

  <xsl:template match="dcterms:dcterms">
  <group>
      <title>Description</title>
	<xsl:apply-templates select="dcterms:identifier.other"><xsl:with-param name="name">Title</xsl:with-param></xsl:apply-templates>
	<xsl:apply-templates select="dcterms:title"><xsl:with-param name="name">Title</xsl:with-param></xsl:apply-templates>
	<xsl:apply-templates select="dcterms:title.alternative"><xsl:with-param name="name">Alternate Title</xsl:with-param></xsl:apply-templates>
	<xsl:apply-templates select="dcterms:identifier.filename"><xsl:with-param name="name">Filename</xsl:with-param></xsl:apply-templates>
	<xsl:apply-templates select="dcterms:relation.isFormatOf"><xsl:with-param name="name">Item identifier</xsl:with-param></xsl:apply-templates>
	<xsl:apply-templates select="dcterms:relation.isPartOf"><xsl:with-param name="name">Hierarchy</xsl:with-param></xsl:apply-templates>
    <xsl:apply-templates select="dcterms:creator"><xsl:with-param name="name">Creator</xsl:with-param></xsl:apply-templates>
    <xsl:apply-templates select="dcterms:contributor"><xsl:with-param name="name">Contributor</xsl:with-param></xsl:apply-templates>
	<xsl:apply-templates select="dcterms:publisher"><xsl:with-param name="name">Publisher</xsl:with-param></xsl:apply-templates>
	<xsl:apply-templates select="dcterms:date.created"><xsl:with-param name="name">Date of Creation</xsl:with-param></xsl:apply-templates>
	<xsl:apply-templates select="dcterms:date.created.range"><xsl:with-param name="name">Date Range of Creation</xsl:with-param></xsl:apply-templates>
	<xsl:apply-templates select="dcterms:coverage.temporal"><xsl:with-param name="name">Date or Period of Content Coverage</xsl:with-param></xsl:apply-templates>
	<xsl:apply-templates select="dcterms:coverage.spatial"><xsl:with-param name="name">Geographic Coverage</xsl:with-param></xsl:apply-templates>
	<xsl:apply-templates select="dcterms:subject"><xsl:with-param name="name">Subject</xsl:with-param></xsl:apply-templates>
	<xsl:apply-templates select="dcterms:type"><xsl:with-param name="name">Record Type</xsl:with-param></xsl:apply-templates>
	<xsl:apply-templates select="dcterms:format.medium"><xsl:with-param name="name">Format Genre</xsl:with-param></xsl:apply-templates>
	<xsl:apply-templates select="dcterms:description.tableOfContents"><xsl:with-param name="name">Arrangement</xsl:with-param></xsl:apply-templates>
	<xsl:apply-templates select="dcterms:rights"><xsl:with-param name="name">Copyright information</xsl:with-param></xsl:apply-templates>
	<xsl:apply-templates select="dcterms:rights.accessRights"><xsl:with-param name="name">Access Restrictions</xsl:with-param></xsl:apply-templates>
	<xsl:apply-templates select="dcterms:description.abstract"><xsl:with-param name="name">Description</xsl:with-param></xsl:apply-templates>
	<xsl:apply-templates select="dcterms:format.extent"><xsl:with-param name="name">Extent</xsl:with-param></xsl:apply-templates>
	<xsl:apply-templates select="dcterms:identifier.bibliographicCitation"><xsl:with-param name="name">Citation Information</xsl:with-param></xsl:apply-templates>
	<xsl:apply-templates select="dcterms:relation.isReferencedBy"><xsl:with-param name="name">TARO URL</xsl:with-param></xsl:apply-templates>
	<xsl:apply-templates select="dcterms:date.digital"><xsl:with-param name="name">Date Digitized</xsl:with-param></xsl:apply-templates>
	<xsl:apply-templates select="dcterms:language"><xsl:with-param name="name">Language</xsl:with-param></xsl:apply-templates>
	<xsl:apply-templates select="dcterms:relation.requires"><xsl:with-param name="name">Technical Requirements</xsl:with-param></xsl:apply-templates>
	<xsl:apply-templates select="dcterms:provenance"><xsl:with-param name="name">Provenance</xsl:with-param></xsl:apply-templates>
	    <xsl:apply-templates select="command"><xsl:with-param name="name">Command</xsl:with-param></xsl:apply-templates>
		<xsl:apply-templates select="unit"><xsl:with-param name="name">Unit</xsl:with-param></xsl:apply-templates>
		<xsl:apply-templates select="bioghist/p"><xsl:with-param name="name">Biographical or Historical Information</xsl:with-param></xsl:apply-templates>
	</group>
  </xsl:template>
  <xsl:template match="dcterms:contributor|
  dcterms:coverage.spatial|
  dcterms:coverage.temporal|
  dcterms:creator|
  dcterms:date.created|
  dcterms:date.created.range|
  dcterms:date.digital|
  dcterms:description.abstract|
  dcterms:description.tableOfContents|
  dcterms:format.extent|
  dcterms:format.medium|
  dcterms:identifier.bibliographicCitation|
  dcterms:identifier.filename|
  dcterms:identifier.other|
  dcterms:language|
  dcterms:provenance|
  dcterms:publisher|
  dcterms:relation.isFormatOf|
  dcterms:relation.isReferencedBy|
  dcterms:relation.requires|
  dcterms:relation.isPartOf|
  dcterms:rights|
  dcterms:rights.accessRights|
  dcterms:subject|
  dcterms:title|
  dcterms:type|
  command|
  unit|
  userestrict|
  bioghist/p">
    <xsl:param name="name"/>
    <item>
      <name><xsl:value-of select="$name"/></name>
      <value><xsl:value-of select="text()"/></value>
    </item>
  </xsl:template>

</xsl:stylesheet>
