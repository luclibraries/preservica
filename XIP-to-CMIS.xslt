<?xml version="1.0" encoding="utf-8" standalone="yes"?>
<!--
  Stylesheet to convert XIP V6.0 metadata to Preservica simple CMIS metadata format.
  This stylesheet does <i>not</i> translate metadata from custom descriptive fragments;
  the Preservica application will retrieve that metadata and appropriate transforms separately.
  However, custom fragments referenced by namespace are available here and may be used
  to generate the title and description.

  The stylesheet is given an XIP document containing only the entity that is being viewed, and
  for an IO, that does not include the representations, COs etc. Metadata fragments associated
  with that entity are included in the document.

  The Preservica simple CMIS metadata format defines a root element named metadata.
  This must contain a mandatory title and description element.
  It can then contain zero or more group or item elements:

  - an item element must contain a name and value element, and can also contain
    an optional type element,
  - a group element must contain a title element, and then one or more item or group elements
    (i.e. groups can be nested within other groups).

  This transform creates a root metadata element for the first entity in the XIP document.

  If the XIP v6 to CMIS transform in the system does not include any xsl:include elements,
  one will be implicitly added for every registered transform in the system for which this
  element has a fragment (for example, if running on an IO with EAD metadata, if there is
  an EAD to CMIS transform registered,  it will be implicitly included.) If you include
  xsl:include elements in this transform, only those transforms will be used.

  Note: This transform uses local names so that it can apply to XIP/v6.0 and also future versions
  of the XIP schema with a different namespace
-->
<xsl:stylesheet version="2.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:oai_dc="http://www.openarchives.org/OAI/2.0/oai_dc/"
				xmlns:ead="urn:isbn:1-931666-22-9"
				xmlns:mods="http://www.loc.gov/mods/v3"
				xmlns:dc="http://purl.org/dc/elements/1.1/"
				xmlns:pbcore="http://www.pbcore.org/PBCore/PBCoreNamespace.html"
                xmlns="http://www.tessella.com/sdb/cmis/metadata"
                >

	<xsl:output method='xml' indent='yes'/>

	<!-- Root template -->
	<xsl:template match="/">
		<xsl:apply-templates/>
	</xsl:template>

	<!-- Override default template for text nodes -->
	<xsl:template match="text()|@*"/>

	<!--
      This template matches the XIP entity elements (SO).
      It outputs a metadata element, blank title and description.
    -->
	<xsl:template match="//*[local-name()='XIP']/*[local-name()='StructuralObject']">
		<metadata>
			<title>
				<xsl:value-of select="*[local-name()='Title']"/>
			</title>
			<description>
				<xsl:call-template name="folderDescription">
                  <xsl:with-param name="defaultDescription" select="*[local-name()='Description']"/>
                </xsl:call-template>
			</description>
		</metadata>
	</xsl:template>

	<!--
      This template matches the XIP entity elements (IO, CO).
      It outputs a metadata element, with the title amd description read from the entity.
    -->
	<xsl:template match="//*[local-name()='XIP']/*[local-name()='InformationObject']|//*[local-name()='XIP']/*[local-name()='ContentObject']">
		<metadata>
			<title>				
				<xsl:call-template name="assetTitle">
                  <xsl:with-param name="defaultTitle" select="*[local-name()='Title']"/>
                </xsl:call-template>
			</title>
			<description>
				<xsl:call-template name="folderDescription">
					<xsl:with-param name="defaultDescription" select="*[local-name()='Description']"/>
				</xsl:call-template>
			</description>
		</metadata>
	</xsl:template>
	
	<xsl:template name="assetTitle">
		<xsl:param name="defaultTitle"/>
		<xsl:choose>
			<xsl:when test="//dc:dc/dc:title">
				<xsl:value-of select="//dc:dc/dc:title"/>
			</xsl:when>
			<xsl:when test="//oai_dc:dc/dc:title">
				<xsl:value-of select="//oai_dc:dc/dc:title"/>
			</xsl:when>
			<xsl:when test="//pbcore:pbcoreDescriptionDocument/pbcore:pbcoreTitle">
				<xsl:value-of select="//pbcore:pbcoreDescriptionDocument/pbcore:pbcoreTitle"/>
			</xsl:when>
			<xsl:when test="//ead:ead/ead:archdesc/ead:did/ead:unittitle">
				<xsl:value-of select="//ead:ead/ead:archdesc/ead:did/ead:unittitle"/>
			</xsl:when>
			<xsl:when test="//mods:mods/mods:titleInfo/mods:title">
				<xsl:value-of select="//mods:mods/mods:titleInfo/mods:title"/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="$defaultTitle"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<xsl:template name="folderDescription">
		<xsl:param name="defaultDescription"/>
		<xsl:choose>
			<xsl:when test="//dc:dc/dc:description">
				<xsl:value-of select="//dc:dc/dc:description"/>
			</xsl:when>
			<xsl:when test="//oai_dc:dc/dc:description">
				<xsl:value-of select="//oai_dc:dc/dc:description"/>
			</xsl:when>
			<xsl:when test="//pbcore:pbcoreDescriptionDocument/pbcore:pbcoreDescription">
				<xsl:value-of select="//pbcore:pbcoreDescriptionDocument/pbcore:pbcoreDescription"/>
			</xsl:when>
			<xsl:when test="//ead:ead/ead:archdesc/ead:did/ead:abstract">
				<xsl:value-of select="//ead:ead/ead:archdesc/ead:did/ead:abstract"/>
			</xsl:when>
			<xsl:when test="//mods:mods/mods:abstract">
				<xsl:value-of select="//mods:mods/mods:absract"/>
			</xsl:when>
			<xsl:when test="//dc:dc/dc:title">
				<xsl:value-of select="//dc:dc/dc:title"/>
			</xsl:when>
			<xsl:when test="//oai_dc:dc/dc:title">
				<xsl:value-of select="//oai_dc:dc/dc:title"/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="$defaultDescription"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

</xsl:stylesheet>