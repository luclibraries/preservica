<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="3.0"
    xmlns:pbcore="http://www.pbcore.org/PBCore/PBCoreNamespace.html"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:pres="http://preservica.com/custom/saxon-extensions"
    xmlns:mdl="http://preservica.com/MetadataDropdownLists/v1">

    <xsl:output method="html" encoding="UTF-8"/>

    <!-- Value for this transform name, to select any transform specific text -->
    <xsl:variable name="xslt" select="'pbcore'"/>

    <xsl:include href="common_viewer_templates.xslt"/>

    <!-- If you support custom drop down menu options in your Editor, you should make similar changes to your Viewer.
         The below line should be commented out and example_dropdown_lists.xml should be replaced with the filename of the Metadata dropdown list document which you have uploaded to Preservica -->
    <!-- <xsl:variable name="dropdownlists" as="node()" select='document("example_dropdown_lists.xml")/mdl:Lists[1]'/> -->

    <!-- For each field which has been configured with a custom drop down menu in your editor, you will need to make the following change:
         Within the <xsl:call-template> </xsl:call-template> block, you will need to copy and paste the following parameter (dropDownValues):

         <xsl:with-param name="dropDownValues">
             <xsl:copy-of select='$dropdownlists/mdl:List[@name="nameOfList"]'/>
         </xsl:with-param>

         You will then need to replace nameOfList with the list you have defined in your Metadata dropdown list document -->

    <!-- Main entry point -->
    <xsl:template match="/">
        <div>

            <!-- Put the stylesheet override in here. -->
            <xsl:if test="$cssOverride">
                <link rel="stylesheet" type="text/css">
                    <xsl:attribute name="href">
                        <xsl:value-of select="$cssOverride"/>
                    </xsl:attribute>
                </link>
            </xsl:if>

            <!-- Main Title for page -->
            <span class="XSLTransformTitle">
                <xsl:call-template name="getString">
                    <xsl:with-param name="stringName" select="'mainHeader'"/>
                </xsl:call-template>
            </span>

            <br />
            <table class="XSLTransformTable">
                <col width="200px" />
                <col width="300px" />
                <xsl:call-template name="add-record-information" />
            </table>
        </div>
    </xsl:template>


    <!-- Template for the Record Information -->
    <xsl:template name="add-record-information">

        <!-- Heading for this section -->
        <xsl:call-template name="mainSectionHeaderTemplate">
        </xsl:call-template>

        <!-- The individual fields -->

        <xsl:call-template name="add-field-node">
            <xsl:with-param name="field">
                <path>//Q{http://www.pbcore.org/PBCore/PBCoreNamespace.html}pbcore:pbcoreDescriptionDocument/Q{http://www.pbcore.org/PBCore/PBCoreNamespace.html}pbcore:pbcoreIdentifier</path>
            </xsl:with-param>
        </xsl:call-template>

        <xsl:call-template name="add-field-node">
            <xsl:with-param name="field">
                <path>//Q{http://www.pbcore.org/PBCore/PBCoreNamespace.html}pbcore:pbcoreDescriptionDocument/Q{http://www.pbcore.org/PBCore/PBCoreNamespace.html}pbcore:pbcoreAssetType</path>
            </xsl:with-param>
        </xsl:call-template>

        <xsl:call-template name="add-field-node">
            <xsl:with-param name="field">
                <path>//Q{http://www.pbcore.org/PBCore/PBCoreNamespace.html}pbcore:pbcoreDescriptionDocument/Q{http://www.pbcore.org/PBCore/PBCoreNamespace.html}pbcore:pbcoreAssetDate</path>
            </xsl:with-param>
        </xsl:call-template>

        <xsl:call-template name="add-big-field-node">
            <xsl:with-param name="field">
                <path>//Q{http://www.pbcore.org/PBCore/PBCoreNamespace.html}pbcore:pbcoreDescriptionDocument/Q{http://www.pbcore.org/PBCore/PBCoreNamespace.html}pbcore:pbcoreTitle</path>
            </xsl:with-param>
        </xsl:call-template>

        <xsl:call-template name="add-field-node">
            <xsl:with-param name="field">
                <path>//Q{http://www.pbcore.org/PBCore/PBCoreNamespace.html}pbcore:pbcoreDescriptionDocument/Q{http://www.pbcore.org/PBCore/PBCoreNamespace.html}pbcore:pbcoreSubject</path>
            </xsl:with-param>
        </xsl:call-template>

        <xsl:call-template name="add-big-field-node">
            <xsl:with-param name="field">
                <path>//Q{http://www.pbcore.org/PBCore/PBCoreNamespace.html}pbcore:pbcoreDescriptionDocument/Q{http://www.pbcore.org/PBCore/PBCoreNamespace.html}pbcore:pbcoreDescription</path>
            </xsl:with-param>
        </xsl:call-template>
        
        <xsl:call-template name="add-big-field-node">
            <xsl:with-param name="field">
                <path>//Q{http://www.pbcore.org/PBCore/PBCoreNamespace.html}pbcore:pbcoreDescriptionDocument/Q{http://www.pbcore.org/PBCore/PBCoreNamespace.html}pbcore:pbcoreGenre</path>
            </xsl:with-param>
        </xsl:call-template>

        <xsl:call-template name="add-field-node">
            <xsl:with-param name="field">
                <path>//Q{http://www.pbcore.org/PBCore/PBCoreNamespace.html}pbcore:pbcoreDescriptionDocument/Q{http://www.pbcore.org/PBCore/PBCoreNamespace.html}pbcore:pbcoreCreator/Q{http://www.pbcore.org/PBCore/PBCoreNamespace.html}pbcore:creator</path>
            </xsl:with-param>
        </xsl:call-template>

        <xsl:call-template name="add-field-node">
            <xsl:with-param name="field">
                <path>//Q{http://www.pbcore.org/PBCore/PBCoreNamespace.html}pbcore:pbcoreDescriptionDocument/Q{http://www.pbcore.org/PBCore/PBCoreNamespace.html}pbcore:pbcoreCreator/Q{http://www.pbcore.org/PBCore/PBCoreNamespace.html}pbcore:creatorRole</path>
            </xsl:with-param>
        </xsl:call-template>

        <xsl:call-template name="add-field-node">
            <xsl:with-param name="field">
                <path>//Q{http://www.pbcore.org/PBCore/PBCoreNamespace.html}pbcore:pbcoreDescriptionDocument/Q{http://www.pbcore.org/PBCore/PBCoreNamespace.html}pbcore:pbcoreContributor/Q{http://www.pbcore.org/PBCore/PBCoreNamespace.html}pbcore:contributor</path>
            </xsl:with-param>
        </xsl:call-template>

        <xsl:call-template name="add-field-node">
            <xsl:with-param name="field">
                <path>//Q{http://www.pbcore.org/PBCore/PBCoreNamespace.html}pbcore:pbcoreDescriptionDocument/Q{http://www.pbcore.org/PBCore/PBCoreNamespace.html}pbcore:pbcoreContributor/Q{http://www.pbcore.org/PBCore/PBCoreNamespace.html}pbcore:contributorRole</path>
            </xsl:with-param>
        </xsl:call-template>

        <xsl:call-template name="add-field-node">
            <xsl:with-param name="field">
                <path>//Q{http://www.pbcore.org/PBCore/PBCoreNamespace.html}pbcore:pbcoreDescriptionDocument/Q{http://www.pbcore.org/PBCore/PBCoreNamespace.html}pbcore:pbcoreCoverage/Q{http://www.pbcore.org/PBCore/PBCoreNamespace.html}pbcore:coverage</path>
            </xsl:with-param>
        </xsl:call-template>

        <xsl:call-template name="add-field-node">
            <xsl:with-param name="field">
                <path>//Q{http://www.pbcore.org/PBCore/PBCoreNamespace.html}pbcore:pbcoreDescriptionDocument/Q{http://www.pbcore.org/PBCore/PBCoreNamespace.html}pbcore:pbcoreCoverage/Q{http://www.pbcore.org/PBCore/PBCoreNamespace.html}pbcore:coverageType</path>
            </xsl:with-param>
        </xsl:call-template>

        <xsl:call-template name="add-field-node">
            <xsl:with-param name="field">
                <path>//Q{http://www.pbcore.org/PBCore/PBCoreNamespace.html}pbcore:pbcoreDescriptionDocument/Q{http://www.pbcore.org/PBCore/PBCoreNamespace.html}pbcore:pbcorePublisher/Q{http://www.pbcore.org/PBCore/PBCoreNamespace.html}pbcore:publisher</path>
            </xsl:with-param>
        </xsl:call-template>

        <xsl:call-template name="add-field-node">
            <xsl:with-param name="field">
                <path>//Q{http://www.pbcore.org/PBCore/PBCoreNamespace.html}pbcore:pbcoreDescriptionDocument/Q{http://www.pbcore.org/PBCore/PBCoreNamespace.html}pbcore:pbcorePublisher/Q{http://www.pbcore.org/PBCore/PBCoreNamespace.html}pbcore:publisherRole</path>
            </xsl:with-param>
        </xsl:call-template>

        <xsl:call-template name="add-big-field-node">
            <xsl:with-param name="field">
                <path>//Q{http://www.pbcore.org/PBCore/PBCoreNamespace.html}pbcore:pbcoreDescriptionDocument/Q{http://www.pbcore.org/PBCore/PBCoreNamespace.html}pbcore:pbcoreRightsSummary/Q{http://www.pbcore.org/PBCore/PBCoreNamespace.html}pbcore:rightsSummary</path>
            </xsl:with-param>
        </xsl:call-template>

        <xsl:call-template name="add-field-node">
            <xsl:with-param name="field">
                <path>//Q{http://www.pbcore.org/PBCore/PBCoreNamespace.html}pbcore:pbcoreDescriptionDocument/Q{http://www.pbcore.org/PBCore/PBCoreNamespace.html}pbcore:pbcoreRightsSummary/Q{http://www.pbcore.org/PBCore/PBCoreNamespace.html}pbcore:rightsLink</path>
            </xsl:with-param>
        </xsl:call-template>

        <xsl:call-template name="add-field-node">
            <xsl:with-param name="field">
                <path>//Q{http://www.pbcore.org/PBCore/PBCoreNamespace.html}pbcore:pbcoreDescriptionDocument/Q{http://www.pbcore.org/PBCore/PBCoreNamespace.html}pbcore:pbcoreRightsSummary/Q{http://www.pbcore.org/PBCore/PBCoreNamespace.html}pbcore:rightsEmbedded</path>
            </xsl:with-param>
        </xsl:call-template>

    </xsl:template>

    <!-- Template to deal with name value pair sections -->
    <xsl:template match="pbcore:pbcoreIdentifier | pbcore:pbcoreAssetType | pbcore:pbcoreAssetDate | pbcore:pbcoreTitle | pbcore:creator | pbcore:creatorRole | pbcore:contributor
            | pbcore:contributorRole | pbcore:pbcoreDescription | pbcore:pbcoreSubject | pbcore:pbcoreGenre | pbcore:coverage | pbcore:coverageType | pbcore:publisher | pbcore:publisherRole | pbcore:rightsSummary | pbcore:rightsLink
            | pbcore:pbcoreInstantiation/pbcore:instantiationIdentifier | pbcore:pbcoreInstantiation/pbcore:pbcoreInstantiation | pbcore:pbcoreInstantiation/pbcore:instantiationFileSize
            | pbcore:pbcoreInstantiation/pbcore:instantiationDuration | pbcore:pbcoreInstantiation/pbcore:instantiationAlternativeModes | pbcore:pbcoreInstantiation/pbcore:instantiationLanguage
            | pbcore:rightsSummary | pbcore:rightsLink">
        <xsl:param name="field" />
        <xsl:param name="label" />
        <xsl:param name="dropDownValues" />

        <xsl:choose>
            <xsl:when test="$dropDownValues != ''">
                <xsl:call-template name="setupDDLValidation">
                    <xsl:with-param name="field">
                        <xsl:value-of select="$field"/>
                    </xsl:with-param>
                    <xsl:with-param name="label">
                        <xsl:value-of select="$label"/>
                    </xsl:with-param>
                    <xsl:with-param name="dropDownValues">
                        <xsl:copy-of select='$dropDownValues'/>
                    </xsl:with-param>
                </xsl:call-template>
            </xsl:when>
            <xsl:otherwise>
                <xsl:call-template name="nameValuePairTemplate">
                    <xsl:with-param name="field">
                        <xsl:value-of select="$field"/>
                    </xsl:with-param>
                    <xsl:with-param name="label">
                        <xsl:value-of select="$label"/>
                    </xsl:with-param>
                    <xsl:with-param name="fieldValue">
                        <xsl:value-of select="text()"/>
                    </xsl:with-param>
                </xsl:call-template>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <!-- Template to deal with big value sections -->
    <xsl:template match="pbcore:pbcoreDescription|pbcore:rightsSummary|pbcore:pbcoreTitle">
        <xsl:param name="field"/>
        <xsl:param name="label" />

        <xsl:call-template name="headingAndValueTemplate">
            <xsl:with-param name="field">
                <xsl:value-of select="$field"/>
            </xsl:with-param>
            <xsl:with-param name="label">
                <xsl:value-of select="$label"/>
            </xsl:with-param>
            <xsl:with-param name="fieldValue">
                <xsl:value-of select="text()"/>
            </xsl:with-param>
        </xsl:call-template>
    </xsl:template>

</xsl:stylesheet>